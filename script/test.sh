#!/bin/bash
set -euC

rm -rf ${GOPATH}/pkg/*_js

gometalinter.v2 --config .linter.json ./...

go test -race ./...
