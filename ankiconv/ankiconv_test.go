package ankiconv

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

type extractTest struct {
	Input    string
	Expected string
}

func TestExtractFiles(t *testing.T) {
	tests := []extractTest{
		{
			Input:    `<img src="pt-junho.jpg" />`,
			Expected: `<img src="pt-junho.jpg">`,
		},
		{
			Input:    `[sound:pt-junho.3gp]`,
			Expected: `<audio src="pt-junho.3gp" type="audio/3gpp"></audio>`,
		},
		{
			Input:    `junhoˈʒu.ɲu[sound:pt-junho.3gp]<img src="pt-junho.jpg" /><div>Sexto mês do ano civil.</div>substantivomPortuguês`,
			Expected: `junhoˈʒu.ɲu<audio src="pt-junho.3gp" type="audio/3gpp"></audio><img src="pt-junho.jpg"><div>Sexto mês do ano civil.</div>substantivomPortuguês`,
		},
	}
	for _, test := range tests {
		result, _, err := extractFileMetadata(test.Input)
		if err != nil {
			t.Errorf("Error parsing `%s`: %s\n", test.Input, err)
		}
		if result != test.Expected {
			t.Errorf("Unexpected result parsing '%s':\n\tExpected: %s\n\t  Actual: %s\n", test.Input, test.Expected, result)
		}
	}
}

func TestNormalizeVariableName(t *testing.T) {
	type tt struct {
		in   string
		want string
	}
	tests := testy.NewTable()
	tests.Add("No conversion", tt{"foo", "foo"})
	tests.Add("ASCII whitespace", tt{"foo bar", "foo_bar"})
	tests.Add("many spaces", tt{"foo      bar", "foo_bar"})

	tests.Run(t, func(t *testing.T, tt tt) {
		got := normalizeVariableName(tt.in)
		if got != tt.want {
			t.Errorf("Got: %s, Want: %s\n", got, tt.want)
		}
	})
}
