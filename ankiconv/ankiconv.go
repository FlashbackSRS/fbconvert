package ankiconv

import (
	"bytes"
	"crypto/sha1"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"

	"github.com/flimzy/anki"
	fbmodel "gitlab.com/FlashbackSRS/flashback-model"
)

func int64ToBytes(i int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(i))
	return b
}

// Bundle holds state during the anki-to-flashback conversion process.
type Bundle struct {
	apkg     *anki.Apkg
	b        *fbmodel.Bundle
	now      *time.Time
	cards    []*fbmodel.Card
	owner    *fbmodel.User
	modelMap map[anki.ID]*fbmodel.Model
	noteMap  map[anki.ID]*fbmodel.Note
	deckMap  map[anki.ID]*fbmodel.Deck
	Package  *fbmodel.Package
}

// MarshalJSON implements the json.Marshaler interface for the Bundle type
func (bx *Bundle) MarshalJSON() ([]byte, error) {
	return json.Marshal(bx.Package)
}

// NewBundle returns a new bundle
func NewBundle() *Bundle {
	b := &Bundle{}
	now := time.Now()
	b.now = &now
	b.modelMap = make(map[anki.ID]*fbmodel.Model)
	b.noteMap = make(map[anki.ID]*fbmodel.Note)
	b.deckMap = make(map[anki.ID]*fbmodel.Deck)
	return b
}

// SetNow sets the Bundles' `now` timestamp
func (bx *Bundle) SetNow(now time.Time) {
	bx.now = &now
}

// Convert converts an Anki package to a flasbhack Bundle.
func Convert(name string, o *fbmodel.User, a *anki.Apkg) (*Bundle, error) {
	b := NewBundle()
	return b, b.Convert(name, o, a)
}

func createKey(key ...[]byte) []byte {
	h := sha1.New()
	for _, k := range key {
		_, _ = h.Write(k)
	}
	hash := h.Sum(nil)
	return hash[:]
}

// Convert reads an Anki package with the given name and owner into the waiting
// Bundle.
func (bx *Bundle) Convert(name string, o *fbmodel.User, a *anki.Apkg) error {
	bx.apkg = a
	bx.owner = o
	c, err := bx.apkg.Collection()
	if err != nil {
		return err
	}
	created := time.Time(*c.Created)
	modified := time.Time(*c.Modified)
	ownerID, _ := fbmodel.B32dec(bx.owner.Name)
	id := fbmodel.B32enc(createKey(ownerID, int64ToBytes(created.UnixNano())))
	b, err := fbmodel.NewBundle(id, o.Name)
	if err != nil {
		return err
	}
	b.Created = created
	b.Modified = modified
	b.Name = name
	b.Imported = *bx.now
	bx.Package = &fbmodel.Package{
		Bundle: b,
	}
	bx.b = b
	if err := bx.addThemes(); err != nil {
		return fmt.Errorf("Error converting themes: %s", err)
	}
	if err := bx.addDecks(); err != nil {
		return fmt.Errorf("Error converting decks: %s", err)
	}
	if err := bx.addNotes(); err != nil {
		return fmt.Errorf("Error converting notes: %s", err)
	}
	if err := bx.addCards(); err != nil {
		return fmt.Errorf("Error converting cards: %s", err)
	}
	// 	if err := bx.addReviews(); err != nil {
	// 		return fmt.Errorf("Error converting reviews: %s", err)
	// 	}
	return nil
}

type modelArray []*anki.Model

func (a modelArray) Len() int           { return len(a) }
func (a modelArray) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a modelArray) Less(i, j int) bool { return a[i].ID < a[j].ID }

func (bx *Bundle) addThemes() error {
	c, err := bx.apkg.Collection()
	if err != nil {
		return err
	}
	bx.Package.Themes = make([]*fbmodel.Theme, 0, len(c.Models))
	models := modelArray(make([]*anki.Model, 0, len(c.Models)))
	for _, m := range c.Models {
		models = append(models, m)
	}
	sort.Sort(models)
	for _, aModel := range models {
		t, err := bx.convertTheme(aModel)
		if err != nil {
			return err
		}
		bx.Package.Themes = append(bx.Package.Themes, t)
	}
	return nil
}

// TODO: convert the following fields
// 	Tags           []string          `json:"tags"`  // Anki saves the tags of the last added note to the current model
// 	SortField      int               `json:"sortf"` // Integer specifying which field is used for sorting in the browser

func (bx *Bundle) convertTheme(aModel *anki.Model) (*fbmodel.Theme, error) {
	modified := time.Time(*aModel.Modified)
	ownerID, _ := fbmodel.B32dec(bx.owner.Name)
	id := createKey(ownerID, int64ToBytes(int64(aModel.ID)))
	t, err := fbmodel.NewTheme(fbmodel.EncodeDocID("theme", id))
	if err != nil {
		return nil, err
	}
	t.Name = aModel.Name
	created := aModel.Created()
	t.Created = time.Time(*created)
	t.Modified = modified
	t.Imported = *bx.now
	t.SetFile("$main.css", "text/css", []byte(aModel.CSS))
	for _, filename := range bx.apkg.ListFiles() {
		if filename[0] == '_' {
			cType, err := determineFileType(fileTypes["images"], filename)
			if err != nil {
				return nil, err
			}
			content, err := bx.apkg.ReadMediaFile(filename)
			if err != nil {
				return nil, err
			}
			t.SetFile(filename, cType, content)
		}
	}
	var m *fbmodel.Model
	var masterTemplate *template.Template
	switch aModel.Type {
	case anki.ModelTypeStandard:
		m, _ = t.NewModel(fbmodel.AnkiStandardModel)
		masterTemplate = masterTmpl
	case anki.ModelTypeCloze:
		m, _ = t.NewModel(fbmodel.AnkiClozeModel)
		if len(aModel.Templates) > 1 {
			return nil, errors.Errorf("anki cloze model has %d templates (???!)", len(aModel.Templates))
		}
		masterTemplate = masterClozeTmpl
	}
	m.Name = aModel.Name
	bx.modelMap[aModel.ID] = m
	fields := make(map[int]*anki.Field)
	for _, field := range aModel.Fields {
		fields[field.Ordinal] = field
	}
	for i := 0; i < len(fields); i++ {
		field, ok := fields[i]
		if !ok {
			return nil, errors.New("Anki field missing")
		}
		if err := m.AddField(fbmodel.AnkiField, field.Name); err != nil {
			return nil, err
		}
	}

	tNames := make([]string, len(aModel.Templates))
	templates := make([]*anki.Template, len(aModel.Templates))
	for _, tmpl := range aModel.Templates {
		templates[tmpl.Ordinal] = tmpl
	}
	for i, tmpl := range templates {
		if i != tmpl.Ordinal {
			return nil, errors.Errorf("Out-of-order template. %d != %d", i, tmpl.Ordinal)
		}
		baseName := "!" + aModel.Name + "." + tmpl.Name
		qName := baseName + " question.html"
		aName := baseName + " answer.html"
		tNames[i] = tmpl.Name
		m.Templates = append(m.Templates, tmpl.Name)
		qFormat, err := convertQuestion(tmpl.QuestionFormat)
		if err != nil {
			return nil, errors.Wrap(err, "Error converting question format")
		}
		if e := m.AddFile(qName, fbmodel.TemplateContentType, []byte(qFormat)); e != nil {
			return nil, e
		}
		aFormat, err := convertAnswer(tmpl.AnswerFormat, tmpl.Name+" question.html")
		if err != nil {
			return nil, errors.Wrap(err, "Error converting answer format")
		}
		if e := m.AddFile(aName, fbmodel.TemplateContentType, []byte(aFormat)); e != nil {
			return nil, e
		}
	}
	buf := new(bytes.Buffer)
	if err := masterTemplate.Execute(buf, tNames); err != nil {
		return nil, err
	}
	if err := m.AddFile("$template.0.html", fbmodel.TemplateContentType, buf.Bytes()); err != nil {
		return nil, err
	}

	return t, nil
}

// FIXME: perhaps I can add a conditional comparing $i to .Card.TemplateID, to only
// execute the sub-templates I actually need?
var masterTmpl = template.Must(template.New("template.html").Delims("[[", "]]").Parse(`
[[- range $i, $Name := . -]]
{{if eq [[ $i ]] .Card.TemplateID }}
	<div class="question" data-id="[[ $i ]]">
		{{template "[[ $Name ]] question.html" .Fields }}
	</div>
	<div class="answer" data-id="[[ $i ]]">
		{{template "[[ $Name ]] answer.html" .Fields }}
	</div>
{{ end }}
[[ end -]]
`))

var masterClozeTmpl = template.Must(template.New("template.html").Delims("[[", "]]").Parse(`
	<div class="question" data-id="{{ .Card.TemplateID }}">
		{{template "[[ index . 0 ]] question.html" .Fields }}
	</div>
	<div class="answer" data-id="{{ .Card.TemplateID }}">
		{{template "[[ index . 0 ]] answer.html" .Fields }}
	</div>
`))

var ankiRE = regexp.MustCompile("{{(.*?)}}")

/*
	Anki tags can be:

	{{Foo}}	- Variable substitution
	{{type:Foo}} - Typed field
	{{cloze:Foo}} - Cloze field
	{{cloze::Foo}} - Cloze field (Anki docs mention both formats; so we accept both)
	{{type:cloze:Foo}} - Typed Cloze field
	{{hint:Foo}} - Hint field
	{{#Foo}} - Conditional. Only show if Foo is non-empty.
	{{/Foo}} - Close Conditional.
	{{^Foo}} - Conditional. Only show if Foo is empty.
	{{=<% %>=}} - Change separator.
*/

func convertAnkiTemplate(in string) (string, error) {
	var err error
	out := ankiRE.ReplaceAllStringFunc(in, func(v string) string {
		var r string
		v = v[2 : len(v)-2]
		v = strings.TrimSpace(v)
		switch {
		case strings.HasPrefix(v, "type:cloze:"):
			log.Print("`type:cloze` variables not yet implemented")
			return "[[" + v + "]]"
		case strings.HasPrefix(v, "type:"):
			return fmt.Sprintf(`<input type="text" class="type" name="%s">`, v)
		case strings.HasPrefix(v, "cloze:"):
			r = "cloze ." + strings.TrimPrefix(v[6:], ":")
		case strings.HasPrefix(v, "hint:"):
			log.Print("`hint` variables not yet implemented")
			return "[[" + v + "]]"
		case strings.HasPrefix(v, "#"):
			r = "if ." + v[1:]
		case strings.HasPrefix(v, "^"):
			r = "if not ." + v[1:]
		case strings.HasPrefix(v, "/"):
			r = "end"
		case strings.HasPrefix(v, "="):
			err = errors.New("Change separator not yet implemented")
		case v == "FrontSide": // Special case variable, pass it through
			r = v
		default: // Standard variables
			r = "." + normalizeVariableName(v)
		}
		return "{{" + r + "}}"
	})
	return out, err
}

var vnRE = regexp.MustCompile(`\s+`)

func normalizeVariableName(s string) string {
	return vnRE.ReplaceAllString(s, "_")
}

func convertQuestion(in string) (string, error) {
	return convertAnkiTemplate(in)
}

func convertAnswer(in, q string) (string, error) {
	tmp, err := convertAnkiTemplate(in)
	if err != nil {
		return "", err
	}
	return strings.Replace(tmp, "{{FrontSide}}", fmt.Sprintf(`{{template "%s" .}}`, q), -1), nil
}

type deckArray []*anki.Deck

func (a deckArray) Len() int           { return len(a) }
func (a deckArray) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a deckArray) Less(i, j int) bool { return a[i].ID < a[j].ID }

func (bx *Bundle) addDecks() error {
	c, err := bx.apkg.Collection()
	if err != nil {
		return err
	}
	bx.Package.Decks = make([]*fbmodel.Deck, 0, len(c.Decks))
	decks := deckArray(make([]*anki.Deck, 0, len(c.Decks)))
	for _, d := range c.Decks {
		decks = append(decks, d)
	}
	sort.Sort(decks)
	for _, aDeck := range decks {
		d, err := bx.convertDeck(aDeck)
		if err != nil {
			return err
		}
		bx.Package.Decks = append(bx.Package.Decks, d)
	}
	return nil
}

// TODO: Convert the following fields
// type Deck struct {
//     ExtendedNewCardLimit    int              `json:"extendedNew"`      // Extended new card limit for custom study
//     ExtendedReviewCardLimit int              `json:"extendedRev"`      // Extended review card limit for custom study
//     ConfigID                ID               `json:"conf"`             // ID of option group from dconf in `col` table
//     NewToday                [2]int           `json:"newToday"`         // two number array used somehow for custom study
//     ReviewsToday            [2]int           `json:"revToday"`         // two number array used somehow for custom study
//     LearnToday              [2]int           `json:"lrnToday"`         // two number array used somehow for custom study
//     TimeToday               [2]int           `json:"timeToday"`        // two number array used somehow for custom study (in ms)
//     Config                  *DeckConfig      `json:"-"`
// }

func (bx *Bundle) convertDeck(aDeck *anki.Deck) (*fbmodel.Deck, error) {
	ownerID, _ := fbmodel.B32dec(bx.owner.Name)
	id := createKey(ownerID, int64ToBytes(int64(aDeck.ID)))
	d := fbmodel.NewDeck(fbmodel.EncodeDocID("deck", id))
	created := aDeck.Created()
	d.Created = time.Time(*created)
	d.Modified = time.Time(*aDeck.Modified)
	d.Imported = *bx.now
	d.Name = aDeck.Name
	d.Description = aDeck.Description
	bx.deckMap[aDeck.ID] = d
	// TODO: Handle DeckConfig
	return d, nil
}

func (bx *Bundle) addNotes() error {
	notes, err := bx.apkg.Notes()
	if err != nil {
		return err
	}
	bx.Package.Notes = make([]*fbmodel.Note, 0)
	for notes.Next() {
		aNote, err := notes.Note()
		if err != nil {
			return err
		}
		n, err := bx.convertNote(aNote)
		if err != nil {
			return err
		}
		for i, value := range aNote.FieldValues {
			fv := n.GetFieldValue(i)
			newValue, files, err := bx.extractFiles(value)
			if err != nil {
				return err
			}
			fv.Text = newValue
			for _, file := range files {
				if err := fv.AddFile(file.Filename, file.ContentType, file.Content); err != nil {
					return err
				}
			}
		}
		bx.Package.Notes = append(bx.Package.Notes, n)
	}
	return nil
}

type fileType struct {
	RE      *regexp.Regexp
	Format  string
	TypeMap map[string]string
}

var fileTypes = map[string]fileType{
	"images": {
		RE:     regexp.MustCompile(`<img src="(.*?)"\s*/?>`),
		Format: `<img src="%[1]s">`,
		TypeMap: map[string]string{
			".jpg": "image/jpeg",
			".png": "image/png",
			".gif": "image/gif",
		},
	},
	"audio": {
		RE:     regexp.MustCompile(`\[sound:(.*?)\]`),
		Format: `<audio src="%[1]s" type="%[2]s"></audio>`,
		TypeMap: map[string]string{
			".m4a": "audio/mp4",
			".mp3": "audo/mpeg",
			".ogg": "audio/ogg",
			".oga": "audio/ogg",
			".spx": "audio/ogg",
			".wav": "audio/wav",
			".3gp": "audio/3gpp",
		},
	},
}

// Att represents an Anki attachment
type Att struct {
	Filename    string
	ContentType string
	Content     []byte
}

// extractFiles extracts any file names from the field text, and returns them
// as attachments. It also converts the field markup to valid HTML5.
func (bx *Bundle) extractFiles(value string) (string, []*Att, error) {
	value, files, err := extractFileMetadata(value)
	if err != nil {
		return "", nil, err
	}
	for _, file := range files {
		content, err := bx.apkg.ReadMediaFile(file.Filename)
		if err != nil {
			return "", nil, err
		}
		file.Content = content
	}
	return value, files, nil
}

func determineFileType(typeMap fileType, filename string) (string, error) {
	ext := strings.ToLower(filepath.Ext(filename))
	cType, ok := typeMap.TypeMap[ext]
	if !ok {
		return "", fmt.Errorf("Unable to determine content type for `%s`", filename)
	}
	return cType, nil
}

func extractFileMetadata(value string) (string, []*Att, error) {
	var errs error
	files := make([]*Att, 0, 1)
	for _, ft := range fileTypes {
		value = ft.RE.ReplaceAllStringFunc(value, func(str string) string {
			matches := ft.RE.FindStringSubmatch(str)

			filename := matches[1]
			cType, err := determineFileType(ft, filename)
			if err != nil {
				errs = multierror.Append(errs, err)
			}
			files = append(files, &Att{
				Filename:    filename,
				ContentType: cType,
			})

			return fmt.Sprintf(ft.Format, filename, cType)
		})
	}
	return value, files, errs
}

func (bx *Bundle) convertNote(aNote *anki.Note) (*fbmodel.Note, error) {
	ownerID, _ := fbmodel.B32dec(bx.owner.Name)
	id := createKey(ownerID, int64ToBytes(int64(aNote.ID)))
	n, _ := fbmodel.NewNote(fbmodel.EncodeDocID("note", id), bx.modelMap[aNote.ModelID])
	created := aNote.Created()
	n.Created = time.Time(*created)
	n.Modified = time.Time(*aNote.Modified)
	n.Imported = *bx.now
	bx.noteMap[aNote.ID] = n
	return n, nil
}

func (bx *Bundle) addCards() error {
	cards, err := bx.apkg.Cards()
	if err != nil {
		return err
	}
	bx.Package.Cards = make([]*fbmodel.Card, 0)
	for cards.Next() {
		aCard, err := cards.Card()
		if err != nil {
			return err
		}
		c, err := bx.convertCard(aCard)
		if err != nil {
			return err
		}
		bx.Package.Cards = append(bx.Package.Cards, c)
	}
	return nil
}

func (bx *Bundle) convertCard(aCard *anki.Card) (*fbmodel.Card, error) {
	note := bx.noteMap[aCard.NoteID]
	d, ok := bx.deckMap[aCard.DeckID]
	if !ok {
		return nil, fmt.Errorf("cannot find deck ID %d in map", aCard.DeckID)
	}
	created := aCard.Created()
	c := &fbmodel.Card{
		ID:       fmt.Sprintf("card-%s.%s.%d", strings.TrimPrefix(bx.b.ID, "bundle-"), strings.TrimPrefix(note.ID, "note-"), aCard.TemplateID),
		ModelID:  fmt.Sprintf("%s/%d", note.ThemeID, note.ModelID),
		Created:  time.Time(*created),
		Modified: time.Time(*aCard.Modified),
		Imported: *bx.now,
	}
	d.AddCard(c.ID)
	// 	c.Queue = fb.CardQueue(aCard.Type)
	switch aCard.Queue {
	case anki.CardQueueSuspended:
		c.Suspended = true
		// 	case anki.CardQueueBuried:
		// 		c.Buried = true
		// 	case anki.CardQueueSchedBuried:
		// 		c.AutoBuried = true
		// 	case anki.CardQueueNew, anki.CardQueueLearning, anki.CardQueueReview, anki.CardQueueRelearning:
		// 		cQueue := int(aCard.Queue)
		// 		if cQueue == int(anki.CardQueueRelearning) {
		// 			cQueue = int(anki.CardQueueLearning)
		// 		}
		// 		cType := int(aCard.Type)
		// 		if cQueue != cType {
		// 			return nil, fmt.Errorf("Card Queue %d does not match Type %d", cQueue, cType)
		// 		}
		// 	default:
		// 		return nil, fmt.Errorf("Unknown queue `%d`", aCard.Queue)
	}
	if aCard.Due != nil {
		due := time.Time(*aCard.Due)
		if due.Before(time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC)) {
			return nil, fmt.Errorf("card %d has type %d, queue %d, due %s (%s)", aCard.ID, aCard.Type, aCard.Queue, due, due.String())
		}
		c.Due = fbmodel.Due(due)
	}
	if aCard.Interval != nil {
		c.Interval = fbmodel.Interval(*aCard.Interval)
	}
	c.EaseFactor = aCard.Factor
	c.ReviewCount = aCard.ReviewCount
	c.Deck = d.ID
	// 	c.LapseCount = aCard.Lapses
	return c, nil
}

// func (bx *Bundle) addReviews() error {
// 	return nil
// }
