package fb

import (
	"time"
)

var emptyBundle *Bundle

var shortNow = parseTime("2017-01-01T00:00:00Z")

func init() {
	now = func() time.Time {
		return parseTime("2017-01-01T00:00:00.999Z")
	}
	emptyBundle = &Bundle{ID: "bundle-foo-bar", Created: now(), Modified: now(), Owner: "foo"}
}

const (
	rev1 = "1-xxx"
)

func parseTime(src string) time.Time {
	t, err := time.Parse(time.RFC3339, src)
	if err != nil {
		panic(err)
	}
	return t
}

func parseInterval(i string) Interval {
	iv, err := ParseInterval(i)
	if err != nil {
		panic(err)
	}
	return iv
}

func parseDue(d string) Due {
	du, err := ParseDue(d)
	if err != nil {
		panic(err)
	}
	return du
}
