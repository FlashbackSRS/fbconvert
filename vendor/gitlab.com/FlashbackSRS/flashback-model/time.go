package fb

import (
	"encoding/json"
	"time"
)

// shortTime is just standard time.Time, but always shortens to seconds, and
// converts to UTC, when marshaling.
type shortTime struct {
	time.Time
}

func (t shortTime) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.Time.UTC().Truncate(time.Second))
}
