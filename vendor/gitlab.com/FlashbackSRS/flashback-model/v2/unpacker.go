package v2

import (
	"encoding/json"
	"io"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

// Unpacker is the unpacker that handles v2 (and v1) packages.
type Unpacker struct{}

// Unpack unpacks a version 1 or 2 package.
func (u *Unpacker) Unpack(r io.Reader) (*fb.Package, error) {
	p := new(pkg)
	if err := json.NewDecoder(r).Decode(&p); err != nil {
		return nil, err
	}
	fbPkg := &fb.Package{
		Created:  p.Created,
		Modified: p.Modified,
		Notes:    p.Notes,
		Decks:    p.Decks,
		Themes:   p.Themes,
		Reviews:  p.Reviews,
		Cards:    make([]*fb.Card, len(p.Cards)),
	}
	for i, card := range p.Cards {
		fbCard := *card
		fbPkg.Cards[i] = &fbCard
	}
	if p.Bundle != nil {
		fbPkg.Bundle = &fb.Bundle{
			ID:          p.Bundle.ID,
			Rev:         p.Bundle.Rev,
			Created:     p.Bundle.Created,
			Modified:    p.Bundle.Modified,
			Imported:    p.Bundle.Imported,
			Name:        p.Bundle.Name,
			Description: p.Bundle.Description,
			Owner:       p.Bundle.Owner,
		}
		_ = fbPkg.SetOwner(p.Bundle.Owner)
	}
	return fbPkg, fbPkg.Validate()
}
