package fb

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestPkgValidate(t *testing.T) {
	tests := []validationTest{
		{
			name: "card without deck",
			err:  "card 'card-foo-bar.mViuXQThMLoh1G1Nlc4d_E8kR8o.0' found in package, but not in a deck",
			v: &Package{
				Bundle: emptyBundle,
				Cards: []*Card{
					{
						ID:       "card-foo-bar.mViuXQThMLoh1G1Nlc4d_E8kR8o.0",
						ModelID:  "theme-VGVzdCBUaGVtZQ/0",
						Created:  shortNow,
						Modified: shortNow,
					},
				},
			},
		},
		{
			name: "card missing from package",
			err:  "card 'card-mjxwe-abcde.mViuXQThMLoh1G1Nlc4d_E8kR8o.0' listed in deck, but not found in package",
			v: &Package{
				Bundle: emptyBundle,
				Decks: []*Deck{
					{
						ID:       "deck-AQID",
						Cards:    CardCollection{"card-mjxwe-abcde.mViuXQThMLoh1G1Nlc4d_E8kR8o.0": {}},
						Created:  shortNow,
						Modified: shortNow,
					},
				},
			},
		},
		{
			name: "valid",
			v: &Package{
				Bundle: emptyBundle,
				Decks: []*Deck{
					{
						ID:       "deck-AQID",
						Cards:    CardCollection{"card-foo-bar.mViuXQThMLoh1G1Nlc4d_E8kR8o.0": {}},
						Created:  shortNow,
						Modified: shortNow,
					},
				},
				Cards: []*Card{
					{
						ID:       "card-foo-bar.mViuXQThMLoh1G1Nlc4d_E8kR8o.0",
						ModelID:  "theme-VGVzdCBUaGVtZQ/0",
						Created:  shortNow,
						Modified: shortNow,
					},
				},
			},
		},
		{
			name: "note without matching model",
			err:  "note 'note-Zm9v' has no matching model (theme-Zm9v/3)",
			v: &Package{
				Bundle: emptyBundle,
				Notes: []*Note{
					{
						ID:          "note-Zm9v",
						ThemeID:     "theme-Zm9v",
						ModelID:     3,
						Created:     shortNow,
						Modified:    shortNow,
						Attachments: NewFileCollection(),
					},
				},
			},
		},
	}
	testValidation(t, tests)
}

func TestPkgMarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		pkg      *Package
		expected string
		err      string
	}{
		{
			name: "empty package",
			pkg: &Package{
				Created:  shortNow,
				Modified: shortNow,
				Bundle:   emptyBundle,
			},
			expected: `{
				"created": "2017-01-01T00:00:00Z",
				"modified": "2017-01-01T00:00:00Z",
				"bundle": {
					"_id": "bundle-foo-bar",
					"created": "2017-01-01T00:00:00Z",
					"modified": "2017-01-01T00:00:00Z",
					"type": "bundle"
				}
			}`,
		},
		{
			name: "invalid review",
			pkg: &Package{
				Bundle:  emptyBundle,
				Reviews: []*Review{{}},
			},
			err: "json: error calling MarshalJSON for type *fb.Review: card id required",
		},
		{
			name: "full package",
			pkg: func() *Package {
				theme, _ := NewTheme("theme-abcd")
				model := &Model{
					Theme: theme,
					Type:  "foo",
					Files: theme.Attachments.NewView(),
				}
				theme.ModelSequence = 1
				theme.Models = []*Model{model}

				noteAtt := NewFileCollection()
				return &Package{
					Created:  shortNow,
					Modified: shortNow,
					Bundle: &Bundle{
						ID:       "bundle-mjxwe-mzxw6",
						Owner:    "mjxwe",
						Created:  shortNow,
						Modified: shortNow,
					},
					Themes: []*Theme{theme},
					Decks:  []*Deck{{ID: "deck-ZGVjaw", Created: shortNow, Modified: shortNow, Cards: CardCollection{"card-mjxwe-mzxw6.bmlsCg.0": {}}}},
					Notes:  []*Note{{ID: "note-Zm9v", ThemeID: "theme-abcd", Model: model, Created: shortNow, Modified: shortNow, Attachments: noteAtt}},
					Cards:  []*Card{{ID: "card-mjxwe-mzxw6.bmlsCg.0", ModelID: "theme-abcd/0", Created: shortNow, Modified: shortNow}},
				}
			}(),
			expected: `{
				"created": "2017-01-01T00:00:00Z",
				"modified": "2017-01-01T00:00:00Z",
				"bundle": {"_id":"bundle-mjxwe-mzxw6", "type":"bundle", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z"},
				"themes": [{"_id":"theme-abcd", "type":"theme", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "modelSequence":1, "files":[], "_attachments":{}, "models":[{"fields":null, "files":[], "modelType":"foo", "templates":null, "id":0}]}],
				"decks": [{"_id":"deck-ZGVjaw", "type":"deck", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "cards":["card-mjxwe-mzxw6.bmlsCg.0"]}],
				"notes": [{"_id":"note-Zm9v", "type":"note", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "_attachments":{}, "fieldValues":null, "theme":"theme-abcd", "model":0}],
				"cards": [{"_id":"card-mjxwe-mzxw6.bmlsCg.0", "type":"card", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "model": "theme-abcd/0"}]
			}`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.pkg.MarshalJSON()
			testy.Error(t, test.err, err)
			if d := testy.DiffJSON([]byte(test.expected), result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestSetOwner(t *testing.T) {
	type tst struct {
		input    *Package
		owner    string
		expected *Package
		err      string
	}
	tests := testy.NewTable()
	tests.Add("nil package", tst{
		input: nil,
		err:   "package is nil",
	})
	tests.Add("no bundle", tst{
		input: &Package{},
		err:   "bundle is nil",
	})
	tests.Add("full", tst{
		input: func() *Package {
			theme, _ := NewTheme("theme-abcd")
			model := &Model{
				Theme: theme,
				Type:  "foo",
				Files: theme.Attachments.NewView(),
			}
			theme.ModelSequence = 1
			theme.Models = []*Model{model}

			noteAtt := NewFileCollection()
			return &Package{
				Created:  shortNow,
				Modified: shortNow,
				Bundle:   &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: shortNow, Modified: shortNow},
				Themes:   []*Theme{theme},
				Decks:    []*Deck{{ID: "deck-ZGVjaw", Created: shortNow, Modified: shortNow, Cards: CardCollection{"card-mjxwe-mzxw6.bmlsCg.0": {}}}},
				Notes:    []*Note{{ID: "note-Zm9v", ThemeID: "theme-abcd", Model: model, Created: shortNow, Modified: shortNow, Attachments: noteAtt}},
				Cards:    []*Card{{ID: "card-mjxwe-mzxw6.bmlsCg.0", ModelID: "theme-abcd/0", Created: shortNow, Modified: shortNow}},
			}
		}(),
		owner: "oink",
		expected: func() *Package {
			theme, _ := NewTheme("theme-abcd")
			model := &Model{
				Theme: theme,
				Type:  "foo",
				Files: theme.Attachments.NewView(),
			}
			theme.ModelSequence = 1
			theme.Models = []*Model{model}

			noteAtt := NewFileCollection()
			return &Package{
				Created:  shortNow,
				Modified: shortNow,
				Bundle:   &Bundle{ID: "bundle-oink-mzxw6", Owner: "mjxwe", Created: shortNow, Modified: shortNow},
				Themes:   []*Theme{theme},
				Decks:    []*Deck{{ID: "deck-ZGVjaw", Created: shortNow, Modified: shortNow, Cards: CardCollection{"card-oink-mzxw6.bmlsCg.0": {}}}},
				Notes:    []*Note{{ID: "note-Zm9v", ThemeID: "theme-abcd", Model: model, Created: shortNow, Modified: shortNow, Attachments: noteAtt}},
				Cards:    []*Card{{ID: "card-oink-mzxw6.bmlsCg.0", ModelID: "theme-abcd/0", Created: shortNow, Modified: shortNow}},
			}
		}(),
	})

	tests.Run(t, func(t *testing.T, test tst) {
		err := test.input.SetOwner(test.owner)
		testy.Error(t, test.err, err)
		if d := testy.DiffInterface(test.expected, test.input); d != nil {
			t.Error(d)
		}
	})
}
