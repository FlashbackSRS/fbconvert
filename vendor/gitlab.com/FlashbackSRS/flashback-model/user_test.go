package fb

import (
	"encoding/json"
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestNewUser(t *testing.T) {
	tests := []struct {
		name     string
		id       string
		expected *User
	}{
		{
			name: "valid",
			id:   "mjxwe",
			expected: &User{
				Name:     "mjxwe",
				Created:  shortNow,
				Modified: shortNow,
				Roles:    []string{},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := NewUser(test.id)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestUserMarshalJSON(t *testing.T) {
	type tst struct {
		user *User
		err  string
	}
	tests := testy.NewTable()
	tests.Add("nil user", tst{
		user: NilUser(),
	})
	tests.Add("With optional fields", func() interface{} {
		u := NilUser()
		u.Rev = rev1
		u.FullName = "John Doe"
		u.Email = "jdoe@example.com"

		return tst{
			user: u,
		}
	})
	tests.Add("null fields", tst{
		user: &User{
			Name:     "mjxwe",
			Salt:     "salty",
			Created:  now(),
			Modified: now(),
		},
	})
	tests.Add("all fields", tst{
		user: &User{
			Name:     "mjxwe",
			Roles:    []string{"foo", "bar"},
			Salt:     "salty",
			FullName: "Bob",
			Email:    "bob@bob.com",
			Created:  now(),
			Modified: now(),
		},
	})
	tests.Add("with picture", tst{
		user: &User{
			Name:     "mjxwe",
			Roles:    []string{"foo", "bar"},
			Salt:     "salty",
			FullName: "Bob",
			Email:    "bob@bob.com",
			Created:  shortNow,
			Modified: shortNow,
			HasImage: true,
		},
	})

	tests.Run(t, func(t *testing.T, test tst) {
		result, err := json.Marshal(test.user)
		testy.Error(t, test.err, err)
		if d := testy.DiffAsJSON(testy.Snapshot(t), result); d != nil {
			t.Error(d)
		}
	})
}

func TestUserUnmarshal(t *testing.T) {
	type tst struct {
		input    string
		expected *User
		err      string
	}
	tests := testy.NewTable()
	tests.Add("Empty user", tst{
		input: "{}",
		err:   "Invalid document type for user",
	})
	tests.Add("minimal user", tst{
		input: `{
			"_id": "org.couchdb.user:aaaaaaaaabaabaaaaaaaaaaaaa",
			"type": "user",
			"name": "aaaaaaaaabaabaaaaaaaaaaaaa",
			"created": "2017-01-01T00:00:00Z",
			"modified": "2017-01-01T00:00:00Z",
			"roles": [],
			"userType": ""
			}`,
		expected: &User{
			Name:     "aaaaaaaaabaabaaaaaaaaaaaaa",
			Roles:    []string{},
			Created:  shortNow,
			Modified: shortNow,
		},
	})
	tests.Add("with optional fields", tst{
		input: `{
			"_id": "org.couchdb.user:aaaaaaaaabaabaaaaaaaaaaaaa",
			"_rev": "1-xxx",
			"type": "user",
			"name": "aaaaaaaaabaabaaaaaaaaaaaaa",
			"fullname": "John Doe",
			"email": "jdoe@example.com",
			"created": "2017-01-01T00:00:00Z",
			"modified": "2017-01-01T00:00:00Z",
			"roles": [],
			"userType": ""
			}`,
		expected: &User{
			Name:     "aaaaaaaaabaabaaaaaaaaaaaaa",
			Rev:      rev1,
			FullName: "John Doe",
			Email:    "jdoe@example.com",
			Roles:    []string{},
			Created:  shortNow,
			Modified: shortNow,
		},
	})
	tests.Add("with picture", tst{
		input: `{
			"_id": "org.couchdb.user:aaaaaaaaabaabaaaaaaaaaaaaa",
			"type": "user",
			"name": "aaaaaaaaabaabaaaaaaaaaaaaa",
			"created": "2017-01-01T00:00:00Z",
			"modified": "2017-01-01T00:00:00Z",
			"roles": [],
			"userType": "",
			"_attachments": {
				"picture": {
					"content_type": "image/jpeg",
					"data": "aW1hZ2UgY29udGVudA=="
				}
			}
		}`,
		expected: &User{
			Name:     "aaaaaaaaabaabaaaaaaaaaaaaa",
			Roles:    []string{},
			HasImage: true,
			Created:  shortNow,
			Modified: shortNow,
		},
	})

	tests.Run(t, func(t *testing.T, test tst) {
		result := &User{}
		err := json.Unmarshal([]byte(test.input), &result)
		testy.Error(t, test.err, err)
		if d := testy.DiffInterface(test.expected, result); d != nil {
			t.Error(d)
		}
	})
}

func TestUserDisplayName(t *testing.T) {
	type tst struct {
		user     *User
		expected string
	}
	tests := testy.NewTable()
	tests.Add("full name", tst{
		user:     &User{FullName: "John Doe"},
		expected: "John Doe",
	})
	tests.Add("email", tst{
		user:     &User{Email: "jdoe@example.com"},
		expected: "<jdoe@example.com>",
	})
	tests.Add("Fallback to ID", func(t *testing.T) interface{} {
		user := NewUser("mzxw6")
		return tst{
			user:     user,
			expected: "{org.couchdb.user:mzxw6}",
		}
	})

	tests.Run(t, func(t *testing.T, test tst) {
		result := test.user.DisplayName()
		if result != test.expected {
			t.Errorf("Expected: %s\n  Actual: %s\n", test.expected, result)
		}
	})
}

func TestRandomizeID(t *testing.T) {
	t.Run("already set", func(t *testing.T) {
		u := &User{Name: "foo"}
		err := u.RandomizeID()
		testy.Error(t, "Name already set", err)
	})
	t.Run("success", func(t *testing.T) {
		u := &User{FullName: "bob"}
		err := u.RandomizeID()
		testy.Error(t, "", err)
	})
}

func TestNilUser(t *testing.T) {
	u := NilUser()
	expected := &User{
		Name:     "aaaaaaaaaa",
		Roles:    []string{},
		Created:  shortNow,
		Modified: shortNow,
	}
	if d := testy.DiffInterface(expected, u); d != nil {
		t.Error(d)
	}
}

func TestUserUnmarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected *User
		err      string
	}{
		{
			name:  "invalid json",
			input: "invalid json",
			err:   "failed to unmarshal user: invalid character 'i' looking for beginning of value",
		},
		{
			name:  "wrong type",
			input: `{"type":"chicken"}`,
			err:   "Invalid document type for user",
		},
		{
			name: "null fields",
			input: `{
                "_id":      "org.couchdb.user:mjxwe",
                "name":     "mjxwe",
                "type":     "user",
                "salt":     "salty",
                "password": "abc123",
                "username": "bob",
                "created":  "2017-01-01T00:00:00Z",
                "modified": "2017-01-01T00:00:00Z"
            }`,
			expected: &User{
				Name:     "mjxwe",
				Salt:     "salty",
				Created:  shortNow,
				Modified: shortNow,
			},
		},
		{
			name: "all fields",
			input: `{
                "_id":        "org.couchdb.user:mjxwe",
                "name":       "mjxwe",
                "type":       "user",
                "salt":       "salty",
                "password":   "abc123",
                "email":      "bob@bob.com",
                "fullname":   "Bob",
                "created":    "2017-01-01T00:00:00Z",
                "modified":   "2017-01-01T00:00:00Z"
            }`,
			expected: &User{
				Name:     "mjxwe",
				Salt:     "salty",
				Email:    "bob@bob.com",
				FullName: "Bob",
				Created:  shortNow,
				Modified: shortNow,
			},
		},
		{
			name:  "wrong doc id format",
			input: `{"_id":"foo", "type":"user"}`,
			err:   "id must have 'org.couchdb.user:' prefix",
		},
		{
			name:  "name-id mismatch",
			input: `{"_id":"org.couchdb.user:foo", "name":"bar", "type":"user"}`,
			err:   "user name and id must match",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := &User{}
			err := result.UnmarshalJSON([]byte(test.input))
			testy.Error(t, test.err, err)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestUserID(t *testing.T) {
	u := NewUser("foo")
	expected := "org.couchdb.user:foo"
	if id := u.ID(); id != expected {
		t.Errorf("Unexpected id: %s", id)
	}
}
