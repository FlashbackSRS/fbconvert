package testy

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestJSONDir(t *testing.T) {
	type tt struct {
		dir JSONDir
		err string
	}
	tests := NewTable()
	tests.Add("empty dir", func(t *testing.T) interface{} {
		tmpdir, err := ioutil.TempDir("", "testy")
		if err != nil {
			t.Fatal(err)
		}
		return tt{
			dir: JSONDir{Path: tmpdir},
		}
	})
	tests.Add("single file", tt{
		dir: JSONDir{Path: "testdata/dirs/singlefile"},
	})
	tests.Add("no MD5 sum", tt{
		dir: JSONDir{
			Path:     "testdata/dirs/singlefile",
			NoMD5Sum: true,
		},
	})
	tests.Add("with contents", tt{
		dir: JSONDir{
			Path:        "testdata/dirs/singlefile",
			NoMD5Sum:    true,
			FileContent: true,
		},
	})
	tests.Add("no size", tt{
		dir: JSONDir{
			Path:        "testdata/dirs/singlefile",
			NoMD5Sum:    true,
			FileContent: true,
			NoSize:      true,
		},
	})
	tests.Add("max content length", tt{
		dir: JSONDir{
			Path:           "testdata/dirs/largefile",
			NoMD5Sum:       true,
			FileContent:    true,
			MaxContentSize: 100,
		},
	})
	tests.Add("detect content type", tt{
		dir: JSONDir{
			Path:           "testdata/dirs/ct",
			NoMD5Sum:       true,
			FileContent:    true,
			MaxContentSize: 100,
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result, err := json.Marshal(tt.dir)
		Error(t, tt.err, err)
		if d := DiffAsJSON(&File{Path: "testdata/" + Stub(t)}, result); d != nil {
			t.Error(d)
		}
	})
}
